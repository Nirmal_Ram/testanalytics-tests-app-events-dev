"use strict";
const MongoClient = require('mongodb').MongoClient;
const MONGODB_URI = "mongodb+srv://nirmal_readonly:tZJ558kAJ4Epdwcl@cluster0.dlbyh.mongodb.net/event-data?authSource=admin&replicaSet=atlas-yjhvlo-shard-0&readPreference=primary&appname=MongoDB%20Compass&ssl=true";
let dbInstance = null;
module.exports.get = async function () {
  if (dbInstance) {
    return Promise.resolve(dbInstance);
  }
  const db = await MongoClient.connect(MONGODB_URI);
  dbInstance = db.db("event-data");
  return dbInstance;
}