"use strict";
const dbjs = require("./db.js");
exports.handler = async (event) => {
  const db = await dbjs.get();

  const data = await db
    .collection("hotstar-event")
    .find({ testId: event.params.path.testUUID })
    .project({ _id: 0 })
    .toArray();

  let result = {
    version: "1.2",
    uuid: {
      _id: "",
      testId: "",
      sessionId: "",
      userDisplayName: "",
      userEmail: "",
      username: "",
    },
    events: data.map((test) => {
      return {
        eventName: test.eventName,
        time: test.eventAttributes.dateTime,
        testCase: test.eventAttributes.testCaseName,
        kpiName: "",
      };
    }),
  };

  const response = {
    statusCode: 200,
    body: result,
    headers: {
      "Access-Control-Allow-Headers":
        "Content-Type, Origin, X-Requested-With, Accept, Authorization, Access-Control-Allow-Methods, Access-Control-Allow-Headers, Access-Control-Allow-Origin",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "OPTIONS,POST,GET,PUT",
    },
  };
  return response;
};
